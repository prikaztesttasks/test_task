﻿// Не знаю смысла использовать boost::unique_ptr, заиспользовал std::unique_ptr вместо boost::unique_ptr
//
// Большой список параметров смотрится плохо для восприятия программистом который использует данный класс
// Логика запрета использования частей также неявная, что затруднит использование
// 
// Предлагаю:
// Вынести список частей в отдельный класс
// Явно добавить логику обработки ошибок при неправильном добавлении элементов
// 
// Ниже код примера
//

#include <iostream>
#include <vector>
#include <variant>
#include <unordered_map>

class IEngine {};
class IArmor {};
class IArm {};
class IWeapon 
{
public:
    virtual void shoot() = 0;
};
class ILegs {};
class IWheels {};

class NuclearEngine : public IEngine {};
class IronArm : public IArm {};
class LaserShotgun : public IWeapon 
{
public:
    virtual void shoot()
    {
        std::cout << "Laser shoot" << std::endl;
    }
};
class SpikedWheels : public IWheels {};

// Для идентификации элементов RobotParts
enum class PartKey
{
    Engine,
    Armor,
    LeftHand,
    RightHand,
    LeftWeapon,
    RightWeapon,
    Legs,
    Wheels
};

// Возможные части роботов
using RobotPartType = std::variant<
    std::unique_ptr<IEngine>, 
    std::unique_ptr<IArmor>, 
    std::unique_ptr<IArm>, 
    std::unique_ptr<IWeapon>, 
    std::unique_ptr<ILegs>, 
    std::unique_ptr<IWheels>>;

class RobotParts
{
public:
    enum class PositionType
    {
        Left,
        Right
    };

    RobotParts()
    {}

    RobotParts(RobotParts&& other)
        : parts(std::move(other.parts))
    {
        
    } 

    // Методы для добавления элементов
    // Явная логика работы с неправильными ситуациями
    // Должны бросать исключение при ошибке
    // Легко добавить новый метод для новой части
    // 

    void add(std::unique_ptr<IEngine>&& engine)
    {
        if (parts.count(PartKey::Engine))
            throw std::logic_error("Already contains engine");
        parts[PartKey::Engine] = std::move(engine);           
    }
    void add(std::unique_ptr<IArmor>&& armor)
    {
        if (parts.count(PartKey::Armor))
            throw std::logic_error("Already contains armor");
        parts[PartKey::Armor] = std::move(armor);
    }
    void add(std::unique_ptr<IArm>&& arm, PositionType pos) 
    {
        PartKey key = (pos == PositionType::Right) ? PartKey::RightHand : PartKey::LeftHand;
        if (parts.count(key))
            throw std::logic_error("Already contains hand");
        parts[key] = std::move(arm);
    }
    void add(std::unique_ptr<IWeapon>&& weapon, PositionType pos) 
    {
        PartKey key = (pos == PositionType::Right) ? PartKey::RightWeapon : PartKey::LeftWeapon;
        if (parts.count(key))
            throw std::logic_error("Already contains weapon");
        PartKey handKey = (pos == PositionType::Right) ? PartKey::RightHand : PartKey::LeftHand;
        if (!parts.count(handKey))
            throw std::logic_error("Cant add weapon without hand");
        parts[key] = std::move(weapon);
    }
    void add(std::unique_ptr<ILegs>&& legs) 
    {
        if (parts.count(PartKey::Legs))
            throw std::logic_error("Already contains legs");
        if (parts.count(PartKey::Wheels))
            throw std::logic_error("Already contains wheels, cant add legs");
        parts[PartKey::Legs] = std::move(legs);
    }
    void add(std::unique_ptr<IWheels>&& wheels) 
    {
        if (parts.count(PartKey::Wheels))
            throw std::logic_error("Already contains wheels");
        if (parts.count(PartKey::Legs))
            throw std::logic_error("Already contains legs, cant add wheels");
        parts[PartKey::Wheels] = std::move(wheels);
    }

    // Метод получения части
    const RobotPartType& getPart(PartKey key) 
    {
        if (!parts.count(key))
            throw std::logic_error("Dont have part for this key");
        return parts.at(key);
    }

private:
    std::unordered_map<PartKey, RobotPartType> parts;
};

class Robot 
{
public:
    Robot(RobotParts&& parts_) : parts(std::move(parts_)) {}
    void shoot(double x, double y)
    {
        /* ... */
        // пример стрельбы
        try
        {
            auto& weaponPart = parts.getPart(PartKey::RightWeapon);
            auto& weaponPtr = std::get<std::unique_ptr<IWeapon>>(weaponPart);
            weaponPtr->shoot();
        }
        catch (const std::bad_variant_access& ex)
        {
            std::cout << ex.what() << std::endl;
        }
        catch (const std::logic_error& ex)
        {
            std::cout << ex.what() << std::endl;
        }
        catch (...)
        {
            /* ... */
            std::cout << "something wrong" << std::endl;
        }
    }
    void move(double x, double y) { /* ... */ }
private:
    // ...
    RobotParts parts;
};

std::vector<Robot> prepare_level_one_enemies() 
{
    std::vector<Robot> enemies;
    // Добавления нормального робота
    try 
    {
        RobotParts parts;
        parts.add(std::make_unique<NuclearEngine>());
        parts.add(std::make_unique<IronArm>(), RobotParts::PositionType::Left);
        parts.add(std::make_unique<LaserShotgun>(), RobotParts::PositionType::Left);
        parts.add(std::make_unique<SpikedWheels>());

        enemies.push_back(Robot(std::move(parts)));
    }
    catch (const std::logic_error& e)
    {
        // Process
    }
    // Неправильное добавление робота
    // В случае неправильного создания программист явно увидит исключение и поймет что идет не так
    try
    {
        RobotParts parts;
        parts.add(std::make_unique<LaserShotgun>(), RobotParts::PositionType::Left);

        enemies.push_back(Robot(std::move(parts)));
    }
    catch (const std::logic_error& e)
    {
        // Process
        std::cout << e.what() << std::endl;
    }
    // ...
    return enemies;
}

int main()
{
    auto robots = prepare_level_one_enemies();
    std::cout << "Done!" << std::endl;
    return 0;
}
